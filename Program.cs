﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Multi_threadedProgrammingTest
{
    class Program
    {
        #region Question 3
        static int Add(int a, int b)
        {
            return a + b;
        }
        public delegate int AddTwoNumbers(int a, int b);
        public static int InvocationDelegate(AddTwoNumbers f, int a, int b)
        {
            return f(a, b);
        }
        #endregion
        #region Question 5
        public class MinusBalanceEventArgs : EventArgs
        {
            public string Name { get; set; }
            public double Balance { get; set; }
        }
        static event EventHandler<MinusBalanceEventArgs> NoMoney;
        static void NegativeBalacneAlart(object sender, MinusBalanceEventArgs e)
        {
            Console.WriteLine($"{e.Name}, you have {e.Balance}$ in your bank account");
        }
        static void GetMoney(double money)
        {
            string name = "Danny";
            double balace = 1000;
            balace -= money;
            if (balace < 0)
                if (NoMoney != null)
                    NoMoney.Invoke(new object(), new MinusBalanceEventArgs { Name = name, Balance = balace });

        }
        #endregion
        #region Question 7
        public class DishEventArgs : EventArgs
        {
            public string DishName { get; set; }
        }
        public class Kitchen
        {
            public event EventHandler<DishEventArgs> DishReady;
            public void PrepareDish()
            {
                Console.WriteLine("Preparing dish...");
                if (DishReady != null)
                    DishReady.Invoke(new object(), new DishEventArgs { DishName = "Falafel" });
            }
        }
        public class Waiter
        {
            public void OnDishReady(object sender, DishEventArgs e)
            {
                Console.WriteLine($"Serving the dish {e.DishName} to the customers");
            }
        }
        #endregion
        #region Question 9
        static void LongOperation()
        {
            for (int i = 0; i < 1000000000; i++)
            {

            }
            Console.WriteLine("Done");
        }
        #endregion
        #region Question 14
        static void Download()
        {
            Console.WriteLine("Downloading file...");
            Thread.Sleep(10000);
            Console.WriteLine("Completed");
        }
        static void Calculate(object obj)
        {
            Console.WriteLine(1.1 * 2.5);
        }
        #endregion
        #region Question 19
        static object key = new object();
        static object key1 = new object();
        static void DoctorTreatment()
        {
            Console.WriteLine("Waiting for my turn...");
            lock (key)
            {
                Thread.Sleep(1000);
                Monitor.Pulse(key);
                Console.WriteLine("Getting treatment");
                NurseCheck();
            }
        }
        static void NurseCheck()
        {
            lock (key1)
            {
                Console.WriteLine("Nurse is checking");
                Thread.Sleep(5000);
                Console.WriteLine("Next Patient please!");
                Monitor.Pulse(key1);
            }
        }
        #endregion
        #region Question 22
        //static ManualResetEvent host = new ManualResetEvent(false);
        static AutoResetEvent host = new AutoResetEvent(false);
        static void EnterClub()
        {
            Console.WriteLine("Waiting to enter...");
            host.WaitOne();
            Console.WriteLine($"Thread {Thread.CurrentThread.ManagedThreadId} enterd to the club");
        }
        #endregion
        #region Question 33
        static CancellationTokenSource source = new CancellationTokenSource();
        static long MyTimer(long count)
        {
            while (!source.IsCancellationRequested)
            {
                count++;
                Thread.Sleep(1000); //I added sleep to show realistic result of timer
            }
            return count;
        }
        #endregion
        #region Question 34
        
        public class Workers
        {
            List<Task> tasksToRun = new List<Task>(5);
            private object key = new object();
            Queue<int> queue = new Queue<int>();
            public Workers(Queue<int> work, int workersNum = 5)
            {
                for (int i = 0; i < workersNum; i++)
                {
                    tasksToRun.Add(Task.Run(() =>
                    {
                        while (true)
                        {
                            int work_num = work.Dequeue();
                            try
                            {
                                while (work.Count == 0)
                                    Monitor.Wait(key);
                                work_num = work.Dequeue();
                            }
                            finally
                            {
                                Monitor.Exit(key);
                            }
                        }
                    }));
                }
            }
            public void AddWork(int num)
            {
                lock(key)
                {
                    queue.Enqueue(num);
                    Console.WriteLine(num * 2);
                    Monitor.PulseAll(key);
                }
            }

        }
        #endregion
        #region Question 38
        static async Task ReadAsync()
        {
            string filename = @"C:\projects\HackerU\text.txt";
            byte[] result;

            using (FileStream SourceStream = File.Open(filename, FileMode.Open))
            {
                result = new byte[SourceStream.Length];
                await SourceStream.ReadAsync(result, 0, (int)SourceStream.Length);
            }

            Console.WriteLine(Encoding.ASCII.GetString(result));
        }
        #endregion
        static void Main(string[] args)
        {
            // Question 1
            /*
            List<int> list = new List<int>();
            Random random = new Random();
            for (int i = 0; i < 100; i++)
            {
                list.Add(random.Next(0, 51));
            }
            var lst = list.Where(x => x < 10);
            var lst1 = list.Where(x => x % 3 == 0);
            var lst2 = list.FindAll(x => x > 20 && x % 2 == 0 );
            var lst3 = list.OrderByDescending(x => x);
            */

            // Question 2
            /*
            List<string> names = new List<string>()
            {
                "Roey","Itay","Ben","Moshe","Daniel","Adi","Shiri","Ofer","Dan","Gili"
            };
            var lst = names.Where(name => name.Length > 4);
            var lst1 = names.Where(name => name.Contains("a")|| name.Contains("A"));
            var lst2 = names.OrderBy(x => x);
            */

            //Question 3
            //int num = InvocationDelegate(Add, 1, 2);

            //Question 5
            /*
            NoMoney += NegativeBalacneAlart;
            GetMoney(5000);
            */

            //Question 7
            /*
            Kitchen kitchen = new Kitchen();
            Waiter waiter = new Waiter();
            kitchen.DishReady += waiter.OnDishReady;
            kitchen.PrepareDish();
            */

            //Question 9
            /*
            Stopwatch sw = new Stopwatch();
            sw.Start();
            for (int i = 0; i < 5; i++)
            {
                Thread thread = new Thread(() => LongOperation());
                thread.Start();
                //thread.Join();
            }
            //for (int i = 0; i < 5; i++)
            //{
            //    LongOperation();
            //}
            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds);
            */

            //Question 12
            /*
            Thread thread = new Thread(() => Console.WriteLine("Hello World"));
            List<int> list = new List<int>() { 1, 2, 3, 4, 5 };
            Thread thread1 = new Thread((num) =>
              {
                  if (list.Contains((int)num))
                      Console.WriteLine("found");
                  else
                      Console.WriteLine("Not found");
              });
            thread1.Start((object)1);
            */

            //Question 14
            /*
            Thread thread = new Thread(Download);
            thread.Start();
            ThreadPool.QueueUserWorkItem(new WaitCallback(Calculate));
            */

            //Question 19
            /*
            for (int i = 0; i < 5; i++)
            {
                Thread thread = new Thread(() => DoctorTreatment());
                thread.Start();
            }
            */

            //Question 22
            /*
            for (int i = 0; i < 50; i++)
            {
                Thread thread = new Thread(() => EnterClub());
                thread.Start();
            }
            Thread.Sleep(3000);
            //host.Set(); //for manual reset event case
            for (int i = 0; i < 50; i++)
            {
                host.Set(); //for auto reset event case
                Thread.Sleep(1000);
            }
            */

            //Question 25
            /*
            Task task = Task.Factory.StartNew(() =>
              {
                  Thread.Sleep(10000);
              }, TaskCreationOptions.LongRunning);
            task.Wait();
            */

            //Question 26
            /*
            Task<int> result = Task<int>.Factory.StartNew(() => 1+1);
            result.Wait();
            */

            //Question 27
            /*
            Task t1 = Task.Run(() =>
              {
                  Console.WriteLine("Welcome");
              });
            t1.Wait();
            Task t2 = new Task(() =>
              {
                  Console.WriteLine("Goodbye");
              });
            t2.RunSynchronously();
            */

            //Question 28
            /*
            Task task = Task.Run(() =>
              {
                 Console.WriteLine(DateTime.Now.Hour.ToString());
              }).ContinueWith((Task Dad) =>
              {
                 Console.WriteLine(DateTime.Now.Hour.ToString());
              },TaskContinuationOptions.NotOnFaulted);
            task.Wait();
            */

            //Question 29
            /*
            List<Task> tasks = new List<Task>();
            for (int i = 0; i < 3; i++)
            {
                Task task = new Task(() =>
                  {
                      Thread.Sleep(5000);
                  });
                tasks.Add(task);
                task.Start();
            }
            Task.WaitAll(tasks.ToArray());
            bool check = true;
            for (int i = 0; i < tasks.Count; i++)
            {
                if (!tasks[i].IsCompleted)
                    check = false;
            }
            string msg = check == true ? "All tasks are done" : "Not all tasks are done";
            Console.WriteLine(msg);
            */

            //Question 30
            /*
            List<Task> tasks = new List<Task>();
            Random rand = new Random();
            for (int i = 0; i < 3; i++)
            {
                Task task = new Task(() =>
                {
                    Thread.Sleep(rand.Next(5, 11) * 1000);
                });
                tasks.Add(task);
                task.Start();
            }
            Task.WaitAny(tasks.ToArray());
            Console.WriteLine("One task is done!");
            */

            //Question 31
            /*
            Task<int> t = Task<int>.Factory.StartNew(() => 
            {
                try
                {
                    int a = 1, b = 0;
                    return a / b;
                }
                catch (DivideByZeroException e)
                {
                    Console.WriteLine("Cannot divide by zero");
                }
                return 0;
            });
            int res = t.Result;
            */

            //Question 33
            /*
            long count = 0;
            Console.WriteLine("Timer is starting");
            Task<long> t = Task<long>.Factory.StartNew(() => MyTimer(count));
            Console.ReadLine();
            source.Cancel();
            Console.WriteLine($"Timer is stopped: {t.Result}");
            */

            //Question 34
            /*
            Queue<int> queue = new Queue<int>(10);
            Workers workers = new Workers(queue);
            workers.AddWork(2);
            workers.AddWork(3);
            workers.AddWork(21);
            workers.AddWork(42);
            workers.AddWork(6);
            workers.AddWork(8);
            workers.AddWork(7);
            workers.AddWork(9);
            workers.AddWork(1);
            workers.AddWork(24);
            */

            //Question 38
            /*
            Task t = ReadAsync();
            t.Wait();
            */
        }
    }
}
